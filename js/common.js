var $ = jQuery;

function switchCreateForm(value) {
    var form = document.getElementById('createNewList');

    if (form) {
        if (value === 'new') {
            form.style.display = 'block';
        } else {
            form.style.display = 'none';
        }
    }
}

$( document ).ready(function() {
    $('select').selectBox().change(function (element) {
        if (element.target.className.includes('choose-want')) {
            switchCreateForm($(this).val());
        }
    });

    $(function () {
        var rangeElement = $("#slider-range");
        var initMin = +rangeElement.attr('init-min') || 0;
        var initMax = +rangeElement.attr('init-max') || 5000;
        var minValue = +rangeElement.attr('min-value') || 0;
        var maxValue = +rangeElement.attr('max-value') || 5000;

        rangeElement.slider({
            range: true,
            min: initMin,
            max: initMax,
            values: [minValue, maxValue],
            slide: function (event, ui) {
                $('input[name="min_price"]').val(ui.values[0]);
                $('input[name="max_price"]').val(ui.values[1]);
                $("#amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#amount").text("$" + rangeElement.slider("values", 0) +
            " - $" + rangeElement.slider("values", 1));

        $('input[name="min_price"]').val(minValue);
        $('input[name="max_price"]').val(maxValue);
    });


    $("#wpb-wps-feature").owlCarousel({
        autoPlay: true,
        stopOnHover: true,
        navigation: false,
        navigationText: ["<i class='wpb-wps-fa-angle-left'></i>", "<i class='wpb-wps-fa-angle-right'></i>"],
        slideSpeed: 1000,
        paginationSpeed: 1000,
        pagination: true,
        paginationNumbers: false,
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        mouseDrag: true,
        touchDrag: true
    });

    $("#wpb-wps-latest-sidebar").owlCarousel({
        autoPlay: true,
        stopOnHover: true,
        navigation: false,
        navigationText: ["<i class='wpb-wps-fa-angle-left'></i>","<i class='wpb-wps-fa-angle-right'></i>"],
        slideSpeed: 1000,
        paginationSpeed: 1000,
        pagination: true,
        paginationNumbers: false,
        items : 1,
        itemsDesktop : [1199,1],
        itemsDesktopSmall : [979,1],
        itemsTablet: [768,1],
        itemsMobile:[479,1],
        mouseDrag: true,
        touchDrag: true,
    });

    $('.clickable-email-title').click(function () {
        if (this.className.search('open') === -1) {
            $('#send-form').slideDown();
            $(this).addClass('open');
        } else {
            $('#send-form').slideUp();
            $(this).removeClass('open');
        }
    });

    $("a[rel='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        allow_resize: true,
        keyboard_shortcuts: true,
        show_title: false,
        social_tools: false,
        autoplay_slideshow: false
    });

});


