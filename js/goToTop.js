window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    var scrollControl = document.getElementById('scrollToTop');

    if (!scrollControl) {
        return;
    }

    if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        scrollControl.style.display = 'block';
    } else {
        scrollControl.style.display = 'none';
    }
}

