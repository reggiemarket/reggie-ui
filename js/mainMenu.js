function switchMenuItem(itemId) {

    if (!itemId) {
        return;
    }

    var $el = document.getElementById(itemId);
    var menuItems;

    if (!$el) {
        return;
    }

    if ($el.classList.contains('current-menu-item')) {
        if ($el.classList.contains('menu-open-item')) {
            $el.classList.remove('menu-open-item');
        } else {
            $el.classList.add('menu-open-item');
        }

        return;
    }

    menuItems = document.getElementsByClassName("menu-item");

    for (var i = 0; i < menuItems.length; i++) {
        menuItems[i].classList.remove('menu-open-item');
        menuItems[i].classList.remove('current-menu-item');
    }

    $el.classList.add('menu-open-item');
    $el.classList.add('current-menu-item');
}

function switchMobileMenuMode() {
    var menu = document.getElementById('left-fixed-menu');

    if (menu) {
        if (menu.classList.contains('open-mobile-menu')) {
            menu.classList.remove('open-mobile-menu');
        } else {
            menu.classList.add('open-mobile-menu');
        }
    }
}

