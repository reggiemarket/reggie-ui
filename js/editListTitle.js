function switchEditableState(isEditable) {
    var viewElement = document.getElementById('viewState');
    var editElement = document.getElementById('editState');

    if (isEditable) {
        viewElement.style.display = 'none';
        editElement.style.display = 'block';
    } else {
        viewElement.style.display = 'block';
        editElement.style.display = 'none';
    }
}
