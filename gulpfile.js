'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var concat = require('gulp-concat');

var jsFiles = [
    './js/libs/jquery.js',
    './js/libs/jquery-migrate.min.js',
    './js/libs/jquery.selectBox.js',
    './js/libs/owl.carousel.js',
    './js/libs/prettyPhoto.js',
    './js/libs/jquery-range-ui.min.js',
    './js/libs/bootstrap.js',
    './js/common.js',
    './js/goToTop.js',
    './js/mainMenu.js',
    './js/editListTitle.js'
];

gulp.task('styles', function () {
    return gulp.src('./less/index.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./static'));
});

gulp.task('scripts', function () {
    return gulp.src(jsFiles)
        .pipe(concat({ path: 'index.js' }))
        .pipe(gulp.dest('./static'));
});


gulp.task('build', ['styles', 'scripts']);
